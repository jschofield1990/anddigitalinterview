﻿using System;
using System.Collections.Generic;
using System.IO;
using AndDigital.Data.Entities;
using Newtonsoft.Json;

namespace AndDigital.Business
{
    public class ProjectAllocation
    {
        public ProjectAllocation()
        {
        }

        public void AllocateEmployees(List<Employee> employees)
        {
            var projects = new List<Project>();
            using (StreamReader r = new StreamReader("../AndDigital.Data/StaticData/Locations.json"))
            {
                Console.WriteLine("getting projects");
                string json = r.ReadToEnd();
                projects = JsonConvert.DeserializeObject<List<Project>>(json);
                Console.WriteLine(projects);
            }

            if (projects == null)
            {
                throw new Exception();
            }

            List<Location> locations = null;
            foreach (var employee in employees)
            {
                var location = new Location();
                var random = new Random();
                int index = random.Next(projects.Count);
                Project chosenProject = projects[index];

                if (chosenProject.StartDate > DateTime.Now || chosenProject.Id == "FG561") // project FG561 is being closed down
                {
                    location.Employee = employee;
                    location.Project = chosenProject;
                    locations.Add(location);
                }
            }

            try
            {
                File.WriteAllText("../AndDigital.Data/StaticData/Locations.json", JsonConvert.SerializeObject(locations));
            }
            catch (Exception ex)
            {
                DoLogging(ex);
            }
        }

        private void DoLogging(Exception exception)
        {
            Console.WriteLine(exception.Message);
        }
    }

    public class Project
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
    }

    public class Location
    {
        public string Identifier { get; set; }
        public Project Project { get; set; }
        public Employee Employee { get; set; }
    }
}
