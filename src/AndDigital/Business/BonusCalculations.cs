﻿using System;
namespace AndDigital.Business
{
    public class BonusCalculations
    {
        public BonusCalculations()
        {
        }

        /// <summary>
        /// Bonus is calculated by using Job Title, Location and Salary.
        /// 1) Leeds 3%, Manchester 5%, Halifax 5%, London 8% of salary
        /// 2) Product Developers 2% of salary, Analysts 1%
        /// 3) Product developers in Manchester an additional 3%
        /// 4) Squad leads are not eligible for a bonus
        /// </summary>
        /// <returns>Total Discount</returns>
        public decimal CalculateBonus()
        {
            return 0;
        }
    }
}
