﻿using System;
using System.Collections.Generic;
using AndDigital.Data.Entities;
using AndDigital.Data.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace AndDigital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        /// <summary>
        /// Get all departments
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IList<Department>> Get()
        {
            var repository = new DepartmentRepository();
            return Ok(repository.Get());
        }

        /// <summary>
        /// Returns specfic department
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetById")]
        public ActionResult<Department> Get(int id)
        {
            throw new NotImplementedException("Needs some work");
        }
    }
}
