﻿using System.Collections.Generic;
using AndDigital.Data.Entities;
using AndDigital.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AndDigital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IList<Employee>> Get()
        {
            var repository = new EmployeeRepository();
            return Ok(repository.Get());
        }

        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns></returns>
        [Route("Filter")]
        [HttpGet]
        public ActionResult<IList<Employee>> Filter(string role, string location)
        {
            var repository = new EmployeeRepository();
            return Ok(repository.Filter(role, location));
        }
    }
}
