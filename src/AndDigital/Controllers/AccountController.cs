﻿using AndDigital.Infrastructure.Models;
using AndDigital.Models;
using AndDigital.Services;
using Microsoft.AspNetCore.Mvc;

namespace AndDigital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        private readonly IAccountService _accountService;

        /// <summary>
        /// Provide credentials for authentication. 
        /// </summary>
        /// <param name="loginDto">Username and Password</param>
        /// <returns>An access and refresh token</returns>
        [HttpPost("login")]
        public ActionResult<AuthenticationResponse> Login(LoginDto loginDto)
        {
            AuthenticationResponse response = _accountService.Login(loginDto);

            if (response == null || response.AccessToken == null)
            {
                return Unauthorized();
            }

            return Ok(response);
        }

        [HttpPost("refreshtoken")]
        public ActionResult RefreshToken()
        {
            //TODO: Need to implement refresh functionality
            return Ok();
        }
    }
}
