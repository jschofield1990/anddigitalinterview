﻿using System;
using System.Collections.Generic;
using System.IO;
using AndDigital.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AndDigital.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        /// <summary>
        /// Get all locations
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IList<Location>> Get()
        {
            try
            {
                using (StreamReader r = new StreamReader("../AndDigital.Data/StaticData/Locations.json"))
                {
                    string json = r.ReadToEnd();
                    List<Location> items = JsonConvert.DeserializeObject<List<Location>>(json);

                    return Ok(items);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return Ok();
            }
        }
    }
}
