﻿using System;
namespace AndDigital.SMTP
{
    public interface ISmptServer
    {
        void SendEmail(string emailAddress, string emailBody);
    }
}
