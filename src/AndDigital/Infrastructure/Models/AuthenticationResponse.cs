﻿namespace AndDigital.Infrastructure.Models
{
    public class AuthenticationResponse
    {
        public AccessToken AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
