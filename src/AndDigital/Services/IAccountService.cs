﻿using AndDigital.Infrastructure.Models;
using AndDigital.Models;

namespace AndDigital.Services
{
    public interface IAccountService
    {
        AuthenticationResponse Login(LoginDto loginDto);
    }
}
