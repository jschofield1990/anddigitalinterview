﻿using System;
using System.Security.Cryptography;

namespace AndDigital.Services
{
    public static class TokenHandler
    {
        public static string GenerateToken(int size = 32)
        {
            var randomNumber = new byte[size];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}
