﻿using System;
using AndDigital.SMTP;

namespace AndDigital.Notifications
{
    public class EmailNotifier
    {
        private readonly ISmptServer _smptServer;

        public EmailNotifier(ISmptServer smptServer)
        {
            _smptServer = smptServer;
        }

        public void Notify(string recipient, string content)
        {
            _smptServer.SendEmail(recipient, content);
        }
    }
}
