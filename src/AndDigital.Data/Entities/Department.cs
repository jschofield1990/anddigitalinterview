﻿using System;
namespace AndDigital.Data.Entities
{
    public class Department
    {
        public Department()
        {
        }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
