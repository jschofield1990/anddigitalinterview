﻿using System;
namespace AndDigital.Data.Entities
{
    public class Employee
    {
        public Employee()
        {
        }

        public int Id { get; set; }

        public string PayrollCode { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public string Location { get; set; }

        public string Department { get; set; }

        public decimal Salary { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime LastModified { get; set; }
    }
}
