﻿using System;
namespace AndDigital.Data.Entities
{
    public class Location
    {
        public Location()
        {
        }

        public string Address { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
    }
}
