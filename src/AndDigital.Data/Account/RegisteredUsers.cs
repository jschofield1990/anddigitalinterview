﻿using System;
using System.Collections.Generic;

namespace AndDigital.Data.Account
{
    public static class RegisteredUsers
    {
        public static IEnumerable<User> Users = new List<User>
        {
            new User { Id = 1, Username = "fred", Password = "123", Role = "Administrator"},
            new User { Id = 2, Username = "alice", Password = "456", Role = "Accountant"},
            new User { Id = 3, Username = "joe", Password = "789", Role = "Guest"},
        };
    }
}
