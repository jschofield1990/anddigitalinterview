﻿using System;
using System.Collections.Generic;
using System.IO;
using AndDigital.Data.Entities;
using Newtonsoft.Json;

namespace AndDigital.Data.Repositories
{
    public class DepartmentRepository
    {
        public DepartmentRepository()
        {
        }

        public IList<Department> Get()
        {
            using (StreamReader r = new StreamReader("../AndDigital.Data/StaticData/Departments.json"))
            {
                string json = r.ReadToEnd();
                List<Department> items = JsonConvert.DeserializeObject<List<Department>>(json);

                return items;
            }
        }
    }
}
