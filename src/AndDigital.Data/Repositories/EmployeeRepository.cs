﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AndDigital.Data.Entities;
using Newtonsoft.Json;

namespace AndDigital.Data.Repositories
{
    public class EmployeeRepository
    {
        public EmployeeRepository()
        {
        }

        public IList<Employee> Get()
        {
            using (StreamReader r = new StreamReader("../AndDigital.Data/StaticData/Employees.json"))
            {
                string json = r.ReadToEnd();
                List<Employee> items = JsonConvert.DeserializeObject<List<Employee>>(json);

                return items;
            }
        }

        public Employee Get(int id)
        {
            return Get().FirstOrDefault(x => x.Id.Equals(id));
        }

        public IList<Employee> Filter(string role, string location)
        {
            var employees = Get();
            if (!string.IsNullOrEmpty(role))
            {
                employees = employees.Where(x => x.Role.Equals(role)).ToList();
            }
            if (!string.IsNullOrEmpty(location))
            {
                employees = employees.Where(x => x.Location.Equals(location)).ToList();
            }

            return employees;
        }

    }
}
