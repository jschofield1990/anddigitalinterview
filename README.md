
# Technical Pair Programming

> The purpose of the solution is to provide a context with examples to provoke technical conversation between an interviewer and a candidate. The exercises are suitable for all PD levels and can be flexed during the session as appropriate.

## The Product
The product is a .Net Core 2.2 web api and its main purpose is to store AND employee information and data associated to employees such as projects and their location. 

### Code from nothing - Recommended for more senior developers
> To test more senior developers it is recommended to ask the dev to code from scratch as this will help understand their capabiltiies. The interviewer can observe how the developer constucts a solution and how they layer and name their projects and files.

- Ask the candidate to create an API with a single resource named Employee. There should be a way of performing standard CRUD operations. In addition the API consumer should be able to retrieve a single entity via a supplied ID.
- There should be a heavy focus on testing and test driven development. There is a requirment for 100% code coverage excluding framework code. For the purpose of this exercise there should be unit tests and integration tests.
- Allow flexibility for data storage. This can be in-memory, file based or even a local SQL DB. This is not important however it should be built in a way that this can easily be replaced in the future.

### Solution Observations/ 10 minutes, look at the faults, provide suggestions

> Ask the candidate to imagine this was their first day working within a product team and they have just cloned the repository. What are their initial observations of the API? Prompt the candidate to image they are participating in a code review, what parts of the solution are good and what could be improved. Ask them what their solution architecture would have been when designing a solution for a basic API project.

**For the Interviewer, some things the candidate might have observed**
- Lack of interface segregation with the use of DI. For example, no IEmployeeRepository, instead the developer has instantiated new instances of the concrete class. **Does the candidate understand the use of DI and how in .net core you can configure services for containers and to register the lifecycle**
- Inconsistent naming conventions on controllers such as a mixture of plural/singular.
- Lack of layering within the Location controller which has logic baked in and not abstracted away into a data layer.
- Location controller catches an exception but returns a 200 status code to the client.

### Project Allocation Refactor 

> The ProjectAllocation class is littered with code smells and is a good candidate for refactoring. Ask the candidate if they can spot any obvious issues and how they would suggest refactoring or improving the current code. This is a good intro into a discussion about code reviews and refactoring. 

**For the Interviewer, some things the candidate might have observed**
 - The obvious issue is that the method is named AllocateEmployees which contradicts with the file name. 
 - A hard coded file path is used to retrieve location data but this is then deserialised into a collection of projects which is incorrect. 
 - There is an attempt to add to a collection which is currently assigned as null which will throw a runtime exception.

### Coding Challenge - Extend API Resource

> Ask the candidate to extend the Employee resource so that a single employee can be returned. There is an existing method in the Employee Repository that can be used to retrieve the data.

**The interviewer should be observing the following**
- Use of correct routing e.g api/Employee/{id}
- Use of correct status codes, check to see if the requested employee exists and if not return a 404 status code.
- Have we considered that we are returning the DB entity to the user and may want to suppress certain values or sensitive info. **How would we handle the mapping between models?**
- Are we challenging the implementation of the repository and are we considering implementing an additional layer.

### Coding Challenge - Extend API Resource with CRUD 

> Ask the candidate to now extend the Employee resource so that the remaining CRUD actions are available.

**The interviewer should be observing the following**
- The correct use of status codes and return types. For example 201 for created with a location header. The newly created entity on a post and no body returned on a delete. **Implementing patch is not required but does the candidate understand when we would use this HTTP methods.**
- Is the request model the same DB entity on POST/PUT or are we looking to implement view models or DTO?

Example of Patch operation
`[{ "op": "test", "path": "/a/b/c", "value": "foo" }]`

### Associated/Sub Resources. 

> Ask the candidate to explain how they would design a resource that returns all employees within a specific department.

**The interviewer should be observing the following**
- Is the endpoint defined correctly, e.g /departments/{departmentId}/employees

### TDD - Bonus Calculation Task 

> Ask the candidate to locate the BonusCalculation class and read the comments. Have a conversation about how they would implement this logic and ask them to consider OOP design. Finally work with the candidate to implement this functionality.

**The interviewer should be observing the following**
- Is the candidate correctly following a TDD approach? 
- Does the code simply implement multiple if/else statements or are we thinking about extensibility?
- Does the code adhere to single responsibility and other SOLID principles?

### Overloading to avoid breaking changes

> Ask the candidate to extend the filtering method within the EmployeeRepository with a new filter option of department without making any breaking changes. The only class which can change is the repository class and no changes are allowed to the controller.

**The interviewer should be observing the following**
- Do we use compile time polymorphism overloading. **Does the candidate understand the difference or can provide examples of compile time and runtime polymorphism such as method overriding for runtime**
- Is the candidate applying nullable params?
- Does the candidate have a different suggestion?

### Notification Service

> Ask the candidate to image they have just picked up a new user story which required the creation of a notification service. The service can use the existing EmailNotifier class for MVP however an SMS notifier option is on the backlog and will shortly follow therefore it is worth considering this in their design.

**The interviewer should be observing the following**
- Is the candidate thinking about SOLID principles and how to handle multiple types of notifiers with the ability to swap a notifier in and out?

### API Authentication & Authorisation

> Ask the candidate to find how authentication has been implementation within the API and for them to explain how this works.

**Further deep-dive**
- Does the candidate understand the different types of authentication such as Basic Authentication, API Keys & OAuth.
- Does the candidate understand how the use of refresh tokes can be useful. How would we implement refresh tokens and what would be the flow for re-authenticating?
- Ask the candidate to restrict access to the employee resource by requiring authentication.
- Ask the candidate to apply authorisation to the employee resource by allowing only admins access.
- Ask the candidate to use either Swagger or postman to authenticate and then use the token to gain access to data.

**The interviewer should be observing the following**
- Are we applying authorisation with the built in attributes e.g [Authorize(Roles = "Administrator")]

### Error Handling

> Ask the candidate to observe the Department/{id} method and how it throws a not implemented exception. A generic error message is returned to the client which is not professional therefore there is a request to change it to simply return "An Internal Server Error Occurred".

**The interviewer should be observing the following**
- Can the candidate find the code which is returning the generic error?
- What is there understanding of the .net core middleware and this implementation of global error handling.
- Do they wrap the controller logic within a try catch?